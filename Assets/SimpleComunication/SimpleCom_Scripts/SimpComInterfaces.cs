﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SimpleCom
{

    public interface SimpComSwitch
    {
        bool Switch { get; set; }

        bool SetSwitch(bool targetLock);
    }

}