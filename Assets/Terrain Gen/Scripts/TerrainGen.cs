﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TerrainGeneration
{
    public class TerrainGen : MonoBehaviour
    {
        public float Xscale = 1f, Xoffset = 0f;
        public float Zscale = 1f, Zoffset = 0f;

        [Range(0, 1)]
        public float baseHeight = .2f;

        public int numPasses = 1;
        public float PassStrength = 1f;
        public float PassStrengthScale = 0.5f;

        public bool regen;

        // Use this for initialization
        void Update()
        {
            if (regen)
            {
                regen = false;

                Terrain terrain = GetComponent<Terrain>();
                TerrainData terrainData = terrain.terrainData;
                int width = terrainData.heightmapWidth;
                int height = terrainData.heightmapHeight;

                float currantStrength = PassStrength;

                float[,] terrainHeights = terrainData.GetHeights(0, 0, width, height);

                for (int x = 0; x < width; x++)
                {
                    for (int z = 0; z < height; z++)
                    {
                        terrainHeights[x, z] = baseHeight;
                    }
                }

                float xWorkingScale = Xscale;
                float zWorkingScale = Zscale;

                for (int pass = 0; pass < numPasses; pass++)
                {
                    // mess with the terrain
                    for (int x = 0; x < width; x++)
                    {
                        for (int z = 0; z < height; z++)
                        {
                            float Xvalue = Xoffset + xWorkingScale * (float)x / width;
                            float Zvalue = Zoffset + zWorkingScale * (float)z / height;

                            float currentHeight = terrainHeights[x, z];

                            currentHeight += currantStrength * (2f * (Mathf.PerlinNoise(Xvalue, Zvalue) - 0.5f));

                            terrainHeights[x, z] = Mathf.Clamp01(currentHeight);
                        }
                    }

                    xWorkingScale *= 1.25f;
                    zWorkingScale *= 1.5f;

                    currantStrength *= PassStrengthScale;
                }

                terrainData.SetHeights(0, 0, terrainHeights);
            }
        }
    }
}
