﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace boids
{

    public class Flock : MonoBehaviour
    {

        public float speed = 0.1f;
        [Range(0, 1)]
        public float ruleUpdatePersentage = 0.2f;
        float rotationRate = 4f;
        Vector3 averageHeading;
        Vector3 averagePosition;
        float neighbourDistance = 3f;

        bool turning = false;

        // Use this for initialization
        void Start()
        {
            newSpeed();
        }

        // Update is called once per frame
        void Update()
        {

            turning = (Vector3.Distance(transform.position, Vector3.zero) >= GlobalFlock.boundSize) ? true : false;

            if (turning)
            {
                Vector3 direction = GlobalFlock.newGoal().normalized - transform.position;
                TurnBoid(direction);
                newSpeed();
            }
            else
            {
                if (Random.Range(0, 1) < ruleUpdatePersentage)
                {
                    ApplyRule();
                }
            }
            transform.Translate(0, 0, Time.deltaTime * speed);
        }

        void ApplyRule()
        {
            GameObject[] allBoids;
            allBoids = GlobalFlock.allBoids;

            Vector3 vCenter = Vector3.zero;
            Vector3 vAvoid = Vector3.zero;
            float gSpeed = 0.1f;

            Vector3 goalPos = GlobalFlock.goalPos;

            float dist;

            int groupSize = 0;
            foreach (GameObject boid in allBoids)
            {
                if (boid != this.gameObject)
                {
                    dist = Vector3.Distance(boid.transform.position, transform.position);
                    if (dist <= neighbourDistance)
                    {
                        vCenter += boid.transform.position;
                        groupSize++;

                        if (dist < 1.0f)
                        {
                            vAvoid += transform.position - boid.transform.position;
                        }

                        Flock anotherFlock = boid.GetComponent<Flock>();
                        gSpeed += anotherFlock.speed;
                    }
                }
            }

            if (groupSize > 0)
            {
                vCenter = vCenter / groupSize + (goalPos - transform.position);
                speed = gSpeed / groupSize;

                Vector3 direction = (vCenter + vAvoid) - transform.position;
                if (direction != Vector3.zero)
                {
                    TurnBoid(direction);
                }
            }
        }

        void TurnBoid(Vector3 direction)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationRate * Time.deltaTime);
        }

        void newSpeed()
        {
            speed = Random.Range(1f, 2);
        }

    }

}
