﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace boids
{
    public class GlobalFlock : MonoBehaviour
    {

        public GameObject boidPrefab;
        public static int boundSize = 25;

        [Range(0.001f, .10f)]
        public float goalChangeOnTick = 0.01f;

        public static int numBoids = 60;
        public static GameObject[] allBoids = new GameObject[numBoids];

        public static Vector3 goalPos = Vector3.zero;

        // Use this for initialization
        void Start()
        {
            for (int index = 0; index < numBoids; index++)
            {
                Vector3 pos = newGoal();
                allBoids[index] = Instantiate<GameObject>(boidPrefab, pos, Quaternion.identity);
            }
        }

        public static Vector3 newGoal()
        {
            return new Vector3(Random.Range(-boundSize, boundSize),
                                          Random.Range(-boundSize, boundSize),
                                          Random.Range(-boundSize, boundSize));
        }

        // Update is called once per frame
        void Update()
        {
            if (Random.Range(0, 1f) < 0.01f)
            {
                goalPos = newGoal();
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(goalPos, .2f);
        }
    }

}
